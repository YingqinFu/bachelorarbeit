# Bachelorarbeit

 After the understanding of the four algorithms from the d-separator problem, we implement practically them with the high-level programming language \textbf{Java} of \textbf{openJDK} version 15.0.1. We generate a directed acyclic graph with Erdős–Rényi model.
