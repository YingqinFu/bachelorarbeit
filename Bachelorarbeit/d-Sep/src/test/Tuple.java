package test;

public class Tuple {
	
	/**
	 * direction f
	 */
	private String f=null;
	
	/**
	 * the node j
	 */
	private Node j=null;
	
	/**
	 * mark of tuple
	 */
	private int Mark=0;
	
	
	public Tuple(String f,Node j) {
		this.f=f;
		this.j=j;
		
	}

	public  void setMark(int Mark) {
		this.Mark=Mark;
	}
	
	public int getMark() {
		return Mark;
		
	}
	
	public  void setDirection(String f) {
		this.f=f;
	}
	
	public String getDirection() {
		return f;
		
	}
	
	public void setNode(Node j) {
		this.j=j;
	}
	
	public Node getNode() {
		return j;
	}
}
