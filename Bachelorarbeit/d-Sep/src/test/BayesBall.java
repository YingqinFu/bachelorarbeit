package test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class BayesBall {
	/**
	 * visit queue	
	 */
	public static Queue<Tuple> toVisit = new LinkedList<Tuple>();
	
	
	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
	
		int[][] G=DAGGenerator.DagGenerator(10,15);
		
		Node[] node = new Node[G.length];
		
		Function.init(node, G);
		
		ArrayList<Node> z=new ArrayList<Node>();
		
		for(int i=1;i<5;i++) {
			z.add(node[i]);
		}
	
		
		 
		 long start=System.currentTimeMillis();
		 
		 System.out.println(testSep(G,node,node[0],node[9],z));
		 
		 long end=System.currentTimeMillis(); 
		 
		 System.out.println("Runtime is��"+(end-start)+"ms");   
		 
		  ArrayList<Node> minSep=FindMinSepNaive.FindMinSepNaive(G,0,9,node); 
		   Function.print(minSep);
	}
    /**
     * function visit
     * @param direction
     * @param x
     */
	public static void Visit(String f,Node j,Node x,ArrayList<Node> reachable){
		
		 Tuple tuple=new Tuple(f,j);
		
		if(tuple.getMark()==0&&j!=x) {
			
			toVisit.offer(tuple);
			
			tuple.setMark(1);
			
			 reachable.add(j);
			
		}
		
	}
	
	/**
	 * function bayes-ball
	 * @param G
	 * @param node
	 * @param x
	 * @param z
	 * @return
	 */
	public static ArrayList<Node> BayesBall (int[][] G, Node[] node,Node x,ArrayList<Node> z){
		
		ArrayList<Node> reachable=new ArrayList<Node>(); 
		
		for(int i=0;i<node.length;i++) {
			Tuple tupleParent=new Tuple("parent",node[i]);
			Tuple tupleChild=new Tuple("child",node[i]);
			tupleParent.setMark(0);
			tupleChild.setMark(0);
			
			
		}
		
		
		toVisit.clear();
		
		
		for(int i=0;i<x.getFather().size();i++) { 
		Visit("child",x.getFather().get(i),x,reachable);
		}
		
		for(int i=0;i<x.getSon().size();i++) {
			Visit("parent",x.getSon().get(i),x,reachable);
		}
		
		 
		 
		while(!toVisit.isEmpty()) {
			Tuple next=toVisit.poll();
			
			if( z.contains(next.getNode())&&next.getDirection().equals("parent")) {
				
				for(int j=0;j<next.getNode().getFather().size();j++) {
					Visit("child",next.getNode().getFather().get(j),x,reachable);
				}
				
			}
			
			if(!z.contains(next.getNode())&&next.getDirection().equals("parent")) {
				
				for(int j=0;j<next.getNode().getSon().size();j++) {
					Visit("parent",next.getNode().getSon().get(j),x,reachable);
				}
			}
			
			if(!z.contains(next.getNode())&&next.getDirection().equals("child")) {
				
				for(int j=0;j<next.getNode().getFather().size();j++) {
					Visit("child",next.getNode().getFather().get(j),x,reachable);
				}
				
				for(int j=0;j<next.getNode().getSon().size();j++) {
					Visit("parent",next.getNode().getSon().get(j),x,reachable);
				}
			}
		}
		
	
		return reachable;
		
	}
	
	
	public static boolean testSep(int[][] G,Node[] node,Node x,Node y,ArrayList<Node> z) {
		
		   ArrayList<Node> reachable=BayesBall(G,node,x,z);
		  
		   return !reachable.contains(y);
		
	}
			
	
	

}
