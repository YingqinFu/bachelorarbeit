package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class DAGGenerator {

	 public static void main(String[] args) {
	    	int[][] G=DagGenerator(10,5);
	    	
	    	//Function.print(G);
	    	
	    }

	 public static int[][] DagGenerator(int n, int m){
		 
		 	int[][] graph = new int[n][n];
			
			int edges=0;
			
			while(edges<m) { 
				
				int u=(int)(Math.random()*graph.length);
				
				int v=(int)(Math.random()*graph.length);
				
				if(u<v&&graph[u][v]==0) {
					
					graph[u][v]=1;
					edges++;
				}
			}
			
			Function.print(graph);
			return graph;
		 
	 }


}
