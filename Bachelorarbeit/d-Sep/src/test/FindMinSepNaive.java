package test;

import java.util.ArrayList;

public class FindMinSepNaive {

	public static void main(String[] args) {

		int[][] G=DAGGenerator.DagGenerator(1000,10000);
	
		Node[] node = new Node[G.length];
	
	  	Function.init(node, G);
		long start=System.currentTimeMillis();

		   ArrayList<Node> minSep=FindMinSepNaive(G,0,800,node); 
		   Function.print(minSep);
			 long end=System.currentTimeMillis(); 
			 
			
			System.out.println("Runtime is: "+(end-start)+"ms");   
	}
	
	
	
	  	public static ArrayList<Node> FindMinSepNaive(int[][] G,int X,int Y,Node[] node) {
	 
		
	  		Node[] z= {node[X],node[Y]};
		   
		   	//get the induce graph of G
			int[][] AntGraph=Ancestor.PaGraph(G,Ancestor.findAn(node,G,z));
			
			Function.printEdges(AntGraph);
			
			 Function.init(node, AntGraph);
			 
			 Node[] z0= {node[X],node[Y]};
			 
			 //get the ancestor of x and y
			 ArrayList<Node> zAntList=Function.toArrayList(Ancestor.findAn(node, AntGraph, z0));
			
			 System.out.println("The number of ancestors of x and y is " +zAntList.size());
			 //Function.print(zAntList);
	     if(!BayesBall.testSep(AntGraph,node, node[X],node[Y], zAntList)) {
	    	  
	        	return null;
	        }
	        
	        else {
	        	
	        	ArrayList<Node> x= Function.copyList(zAntList);
	        	
	        	for(int i=0;i<x.size();i++) {
	        		
	        	x.remove(i);
	        		
	        		if(BayesBall.testSep(AntGraph,node,node[X], node[Y], x)) {
	        		
	        			zAntList.remove(i);
	        			i=i-1;
	        			
	        		}
	        		
	        		x=Function.copyList(zAntList);
	        		
		        	
	        	}
	        }
	        
	     
	      System.out.println("The minimal separator menge between "+X+ " and "+Y+" is: ");
	      
			return zAntList;
	}
}
