package test;

import java.util.ArrayList;

public class Function {

	/**
	 * print the array
	 * @param node
	 */
	public static void print(Node[] node) {
		for(int i=0;i<node.length;i++) {
			System.out.println("Node "+node[i].getName());
		}
	}
	
	
	/**
	 * print the array list
	 * @param z
	 */
	public static void print(ArrayList<Node> z) {
		if(z!=null) {
		for(int i=0;i<z.size();i++) {
			
			System.out.println("Node "+z.get(i).getName());
			
		  }
		}
	}
	
	/**
	 * print the graph
	 */
	public static void print(int[][] G) {
		for(int i=0;i<G.length;i++) {
			 for(int j=0;j<G.length;j++) {
				 System.out.print(G[i][j]+" ");
				 
			 }
			 System.out.println("\n");
		 }
	}
	/**
	 * change a array to ArrayList
	 * @param node
	 * @return
	 */
	public static ArrayList<Node> toArrayList(Node[] node ) {
		ArrayList<Node> nodes = new ArrayList<>();
		for(int i=0;i<node.length;i++) {
			nodes.add(node[i]);
		}
		return nodes;
	}
	
	/**
	 * get the augmented graph of G
	 * @param G
	 * @return
	 */
	public static int[][] moralGraph(int[][] G){
		int[][] noDirectGraph=new int[G.length][G.length];
		noDirected(G, noDirectGraph);
		for(int i=0;i<G.length;i++) {
			for(int j=0;j<G.length;j++) {
				for(int m=j+1;m<G.length;m++) {
				if(G[j][i]==1&&G[m][i]==1) {
					noDirectGraph[j][m]=1;
					noDirectGraph[m][j]=1;
				 }
				}
			}
		}
		return noDirectGraph;
		
	}
	
	/**
	 * remove a node from array
	 * @param arr
	 * @param index
	 * @return
	 */
	public static Node[] remove(Node[] arr, int index) {
		for (int i = index; i < arr.length - 1; i++) {
			arr[i] = arr[i + 1];
		}
		
		arr[arr.length - 1] = null;
		
		Node[] nodes=new Node[arr.length-1];
		
		for(int i=0;i<nodes.length;i++) {
			nodes[i]=arr[i];
		}
		return nodes;
	}
	
	/**
	 * copy the nodes
	 * @param X
	 * @return
	 */
	public static Node[] copyNode(Node[] X) {
		Node[] copy = new Node[X.length];
		for(int i=0;i<copy.length;i++)
		{
			if(X[i]!=null) {
            copy[i] = new Node();
            
			copy[i].setName(X[i].getName());
			}
			else {
				copy[i]=null;
			}
		}
		
		return copy;
	}
	
	/**
	 * change a DAG to no directed graph
	 * @param x
	 * @param y
	 */
	public static void noDirected(int[][] x,int[][] y) {
		
		
		for(int i=0;i<x.length;i++) {
			for(int j=0;j<x.length;j++) {
				y[i][j]=x[i][j];
			  }
			}
		
		for(int i=0;i<x.length;i++) {
			for(int j=0;j<x.length;j++) {
				if(x[i][j]==1) {
					y[j][i]=1;
				}
		    }
		}
	}
	
	/**
	 * initialize the graph, regard G as a no-directed graph
	 * @param node
	 * @param G
	 */
	public static void init(Node[] node,int[][] G) {
		for(int i=0;i<G.length;i++)
		{
            node[i] = new Node();
			node[i].setName(""+i);
		}
		
		
		for(int i=0;i<G.length;i++){
			ArrayList<Node> List = new ArrayList<Node>();
			ArrayList<Node> father = new ArrayList<Node>();
			ArrayList<Node> son = new ArrayList<Node>();
			
			for(int j=0;j<G.length;j++){
				if(G[i][j]==1||G[j][i]==1) {
					List.add(node[j]);
					
				}
				if(G[i][j]==1) {
					son.add(node[j]);
					
				}
				if(G[j][i]==1) {
					father.add(node[j]);
					
				}
				
				
			}
			node[i].setRelationNodes(List);
			node[i].setFather(father);
			node[i].setSon(son);
			List = null;
			father=null;
			son=null;
		}
	}
	/**
	 * assign the cost for all nodes
	 * @param node
	 * @param X
	 * @param Y
	 */
	public static void AssignCost(Node[] node,int X,int Y) {
		
		
		for(int i=0;i<node.length;i++) {
			if(i==X||i==Y) {
				
				node[i].setCost((int)Double.POSITIVE_INFINITY);
			}
			else {
				node[i].setCost(1);
			}
			
		}
		
	}
	/**
	 * copy the node of a List
	 * @param node
	 * @return
	 */
	public static ArrayList<Node> copyList (ArrayList<Node> node){
		ArrayList<Node> copy=new ArrayList<Node>();
		for(int i=0;i<node.size();i++) {
			copy.add(node.get(i));
		}
		return copy;
		
	}
	
	/**
	 * print the edges of a graph
	 * 
	 */
	public static void printEdges(int[][] G) {
		int edges=0;
		for(int i=0;i<G.length;i++) {
			for(int j=0;j<G.length;j++) {
				if(G[i][j]==1) {
					edges++;
				}
			}
		}
		
		System.out.println("The number of edges is "+edges);
	}
	
	/**
	 * print the edges of a graph
	 * 
	 */
	public static void printUndirectedEdges(int[][] G) {
		int edges=0;
		for(int i=0;i<G.length;i++) {
			for(int j=0;j<G.length;j++) {
				if(G[i][j]==1) {
					edges++;
				}
			}
		}
		
		System.out.println("The number of edges is "+edges/2);
	}
	
	
	

}
