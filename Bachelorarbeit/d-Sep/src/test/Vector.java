package test;

import java.util.ArrayList;
import java.util.List;

public class Vector<T> {
	 private T name;
	    private List<Vector<T>> from;
	    private List<Vector<T>> to;
	 
	    public Vector(T name) {
	        this.name = name;
	        this.from = new ArrayList<>();
	        this.to = new ArrayList<>();
	    }
	 
	    public T getName() {
	        return name;
	    }
	 
	    public void setName(T name) {
	        this.name = name;
	    }
	 
	    public List<Vector<T>> getFrom() {
	        return from;
	    }
	 
	    public List<Vector<T>> getTo() {
	        return to;
	    }
}
