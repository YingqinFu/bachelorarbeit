package test;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
 
 
public class Test {
	//stack to save the nodes
	public static Stack<Node> stack = new Stack<Node>();
	//List to save the paths
	public static ArrayList<Object[]> paths = new ArrayList<Object[]>();
 
	//check if the node in the stack or not
	public static boolean isNodeInStack(Node node){
		Iterator<Node> it = stack.iterator();
		while (it.hasNext()) {
			Node node1 = (Node) it.next();
			if (node == node1)
				return true;
		}
		return false;
	}
 
	//print and save the paths
	public static void printAndSavePath(){
		Object[] o = stack.toArray();
		for (int i = 0; i < o.length; i++) {
			Node nNode = (Node) o[i];
			
			if(i < (o.length - 1)) {
				//System.out.print(nNode.getName() + "->");
			}
			else {
				//System.out.print(nNode.getName());
			}
		}
		//save
		paths.add(o); 
		//System.out.println("\n");
	}
 
	/*
	 * The way to find a path
	 * cNode: currentNode
	 * pNode: previousNode
	 * sNode: startNode
	 * eNode: endNode
	 */
	public static boolean getPaths(Node cNode, Node pNode, Node sNode, Node eNode) {
		//get the node now
		Node nNode = null;
		//if there are any circles in the graph,return false
		/*if (cNode != null && pNode != null && cNode == pNode) {
			return false;
		}*/
		if (cNode != null) {
			int i = 0;
			
			//push the current node to stack
			stack.push(cNode);
			
			//if the current node is end node, then a path is found
			if (cNode == eNode){
				//print and save
				printAndSavePath();
				return true;
			}
			//if the current node is not end node, go on and find another path
			else{
				
				//Run through from nodes, which are connected with the current node. Regard it as the start node for next recursion
				nNode = cNode.getRelationNodes().get(i);
				
				//Keep running, untill there are not any related nodes more
				while (nNode != null) {
				
					/*
					 * If nNode is the start node or previous node, or it is already in the stack, which means there is a circle
					 */
					if (pNode != null&& (nNode == sNode || nNode == pNode || isNodeInStack(nNode))) {
						i++;
						if (i >= cNode.getRelationNodes().size()) {
							nNode = null;
						}
						else
							nNode = cNode.getRelationNodes().get(i);
						continue;
					}
					
					//Recursion
					if (getPaths(nNode, cNode, sNode, eNode)){
						
						//pop the paths
						stack.pop();
					}
					//get the nodes, which are related with cNode
					i++;
					
					if (i >= cNode.getRelationNodes().size()) {
						nNode = null;
					}
					else {
						nNode = cNode.getRelationNodes().get(i);
					}
				}
	
				//Find all the paths
				stack.pop();
				return false;
			}
		} 
		else {
			return false;
		}
	}
	
 
	public static void main(String[] args) {
		int[][] G1= {
				{0,1,0,0,0},
				{0,0,1,1,0},
				{0,0,0,1,1},
				{0,0,0,0,0},
				{0,0,0,0,0}
		};
		int[][] G=DAGGenerator.DagGenerator(35,20);
		long start=System.currentTimeMillis();
			//define node
			Node[] node = new Node[G.length];
			 
			//init graph
			Function.init(node, G);
			
			ArrayList<Node> z=new ArrayList<Node>();
			z.add(node[0]);
			z.add(node[2]);
			z.add(node[4]);
			z.add(node[1]);
			
				
			
			System.out.println("The truth, that z is able to separate x and y is: ");
			System.out.println(testSep(G, node[0], node[4], z));
			
			long end=System.currentTimeMillis(); 
			System.out.println("Runtime is��"+(end-start)+"ms");   
	 
	}
	
	
		/**
		 * Bayes-Ball algorithm
		 * @param DAG
		 * @param x
		 * @param y
		 * @param z
		 * @return
		 */
		public static boolean testSep(int[][] DAG,Node x, Node y, ArrayList<Node> z) {
				//get all paths from x to y
				getPaths(x, null, x, y);
				
				//set true, if every node of the path can block the ball from x to y
				boolean [] pathBlock=new boolean[paths.size()];
				
				//Run through all nodes of all paths 
				  for(int i=0;i<paths.size();i++) {
					  
					  //set true, if a single node can block the ball 
					  boolean[] nodeBlock= new boolean[paths.get(i).length]; 
					  
					  for (int j = 0; j < paths.get(i).length; j++) {
						  Node nNode = (Node) paths.get(i)[j];	
							 
						 	//check, if nodes in z are on the paths or not. If z observed or not
						 
						 	//observed nodes
							if(z.contains(nNode)) {
								if(j-1>=0&&j+1<paths.get(i).length) {
									
									//the father node of node j
									Node father= (Node) paths.get(i)[j-1];
									
									//the son node of node j
									Node son= (Node) paths.get(i)[j+1];	
									
									//the Integer of them
									int fa=Integer.parseInt(father.getName());
									int sn=Integer.parseInt(son.getName());
									int now=Integer.parseInt(nNode.getName());
								
									//father-->now-->son(can block the ball)
									if(DAG[fa][now]==1&&DAG[now][sn]==1) {
										
										nodeBlock[j]=true;
									}
									//father<--now-->son(can block the ball)
									if(DAG[now][fa]==1&&DAG[now][sn]==1) {
										
										nodeBlock[j]=true;
										
									}
									//father-->now<--son(cannot block the ball)
									if(DAG[fa][now]==1&&DAG[sn][now]==1) {
										
										nodeBlock[j]=false;
									}
								
							   }
								
								//
								pathBlock[i]=pathBlock[i]|nodeBlock[j];
								
							}
							
							//unobserved nodes
							else {
								if(j-1>=0&&j+1<paths.get(i).length) {
									//the father node of node j
									Node father= (Node) paths.get(i)[j-1];
									
									//the son node of node j
									Node son= (Node) paths.get(i)[j+1];	
									
									//the Integer of them
									int fa=Integer.parseInt(father.getName());
									int sn=Integer.parseInt(son.getName());
									int now=Integer.parseInt(nNode.getName());
								
									
									//father-->now-->son(cannot block the ball)
									if(DAG[fa][now]==1&&DAG[now][sn]==1) {
										
										nodeBlock[j]=false;
									}
									//father<--now-->son(cannot blocj the ball)
									if(DAG[now][fa]==1&&DAG[now][sn]==1) {
										
										nodeBlock[j]=false;
										
									}
									//father-->now<--son(can block the ball)
									if(DAG[fa][now]==1&&DAG[sn][now]==1) {
										
										nodeBlock[j]=true;
									}
								
							   }
								
								pathBlock[i]=pathBlock[i]|nodeBlock[j];
								
							}
					  }
				  }
		
				  	 boolean zSeparatsXandY = true;
				  	 
					 //all paths from x to y
					 for(int n=0;n<paths.size();n++) {
						 
						 zSeparatsXandY=pathBlock[n]&zSeparatsXandY;
						 
					 }
					 
					return zSeparatsXandY; 
		
		
	}
 

}	
