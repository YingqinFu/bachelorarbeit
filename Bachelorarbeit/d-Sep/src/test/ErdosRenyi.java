package test;

public class ErdosRenyi {
	
			public static void main(String[] args) {
					
					int[][] DAG=ErdosRenyiGenerator(5,0.5);
					
					Function.print(DAG);
					
				
					
					
			}
	
			public static int[][] ErdosRenyiGenerator(int N,double prob){
			
			
						int[][] graph = new int[N][N];
			
						int edgs=0;
			
						// Generate an undirected random graph with N nodes and probability p of any two nodes to be connected. 
						
							for(int i=0; i < N; i++){
								for(int j=i+1; j< N; j++){
									double rand = Math.random();
									
									if(rand <= prob){
										graph[i][j]=1;
										edgs++;
										
										//graph[j][i]= 1;
									}
									
								}
								
							}
							
						
						
						System.out.println(edgs);
						
						
						return graph;
		   }
		

}
