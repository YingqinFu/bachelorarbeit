package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class FindMinSepMoral {
	
	
	public static void main(String[] args) {
	
	
		
		int[][] G=DAGGenerator.DagGenerator(500,5500);
		Node[] node = new Node[G.length];
		
		Function.init(node,G);
		
		long start=System.currentTimeMillis();
		
		Function.print(FindMinSepMoral(G,0,400,node));
		
		long end=System.currentTimeMillis(); 
		
		System.out.println("Runtime is: "+(end-start)+"ms");   
		 
	}

	public static ArrayList<Node> FindMinSepMoral(int[][] G,int X,int Y,Node[] node) {
		
		
		   Node[] z= {node[X],node[Y]};
		   
		   //Array of node for ancestor nodes
	       Node[] zArray=Ancestor.findAn(node,G,z);
	       
	       //get the induce graph of G
			int[][] AntGraph=Ancestor.PaGraph(G,zArray);
			
			//get the moral graph of AntGraph
			  int[][] augmentedGraph=Function.moralGraph(AntGraph);
			  
			  
			  Function.init(node, augmentedGraph);
			  
			  Function.printUndirectedEdges(augmentedGraph);
			  
			  Node[] z0= {node[X],node[Y]};
			    
			  //the ancestors of x and y
			  ArrayList<Node> zAntList= Function.toArrayList(Ancestor.findAn(node,AntGraph,z0));
			  
			  System.out.println("The number of ancestors of x and y is " +zAntList.size());
			  
			 if(!BayesBall.testSep(AntGraph,node,node[X], node[Y], zAntList)) {
				
				  return null;
			  }
	       	
			 //Run BFS from X
			 ArrayList<Node> zFromx=FS.BFS(node[X],zAntList);
			 
			//Run BFS from Y
			 ArrayList<Node> Z=FS.BFS(node[Y], zFromx);
			 
			 System.out.println("The minimal separator menge between "+X+ " and "+Y+" is: ");
			 return Z;
		
	}
	
	

}
