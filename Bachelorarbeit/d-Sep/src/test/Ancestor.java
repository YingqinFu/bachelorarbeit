package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Ancestor {
	
	public static ArrayList<Node> List = new ArrayList<Node>();
	
	public static void main(String[] args) {
		/*int[][] G1= {
	    		 {0,1,1,0},//0
	             {0,0,0,1},//1
	             {0,1,0,1},//2
	             {0,0,0,0} //3
		 };*/
		int[][] G2= {
	    		 {0,1,0,0},//0
	             {0,0,1,0},//1
	             {0,0,0,1},//2
	             {0,0,0,0} //3
		 };
		int[][]G= {
				{0,1,0,0,0,0},
				{0,0,1,0,0,0},
				{0,0,0,1,0,0},
				{0,0,0,0,0,1},
				{0,0,0,0,0,0},
				{0,0,0,0,0,0}
		};
		Node[] node = new Node[G.length];
		for(int i=0;i<G.length;i++)
		{
            node[i] = new Node();
			node[i].setName(""+i);
		}

		Node[] z= {node[0],node[5]};
		//findAn(node,G1,z);
		Function.print(findAn(node,G,z));
		PaGraph(G,findAn(node,G,z));
		
		
	}
	/**
	 * Find the ancestors of mNode
	 * @param node
	 * @param G
	 * @param mNode
	 */
	public static Node[] findAn(Node[] node,int[][] G,Node[] mNode) {
		for(int i=0;i<mNode.length;i++) {
			Anc(node,G,mNode[i]);
			
			Set<Node> hs = new HashSet<>();
			hs.addAll(List);
			List.clear();
			List.addAll(hs);
    	
		}
		Node[] Anc = new Node[List.size()];
		
		for (int i = 0; i < List.size(); i++) {
		    Anc[i] = List.get(i);
		}
		List.clear();
		return Anc;

		
	}
	
	
	/**
	 * Find ancestor of a node
	 * @param node
	 * @param G
	 * @param mNode
	 */
	public static void Anc(Node[] node,int[][] G,Node mNode) {
		
			int m = Integer.parseInt(mNode.getName());  
			List.add(mNode);
			for(int j=0;j<G.length;j++)
			{
				if(G[j][m]==1) {
					List.add(node[j]);
					Anc(node,G,node[j]);
					
				}
				
			}
		
			
			
	}
	/**
	 * 
	 * @param Graph
	 * @param subGraph
	 */
	public static int[][] PaGraph(int[][] Graph,Node[] z) {
		 int [][] subGraph=new int[Graph.length][Graph.length];
		 
		 ArrayList<Integer> nodes =new  ArrayList<Integer>();
		 for(int i=0;i<z.length;i++) {
			 nodes.add(Integer.parseInt(z[i].getName()));
		 }
		 
		for(int i=0;i<Graph.length;i++) {
			for(int j=0;j<Graph.length;j++) {
				subGraph[i][j]=Graph[i][j];
			  }
			}
	
		for(int i =0;i<Graph.length;i++) {
			for(int j=0;j<Graph.length;j++) {
				if(!nodes.contains(i)||!nodes.contains(j)) {
					subGraph[i][j]=0;
				}
									
			}
		}
		
		return subGraph;
		
		
	}
	
	/**
	 * copy the nodes
	 * @param X
	 * @return
	 */
	public static Node[] copyNode(Node[] X) {
		Node[] copy = new Node[X.length];
		for(int i=0;i<copy.length;i++)
		{
            copy[i] = new Node();
			copy[i].setName(X[i].getName());
		}
		return copy;
	}
	
	


}
