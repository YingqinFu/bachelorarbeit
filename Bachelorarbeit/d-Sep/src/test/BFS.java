package test;
import java.util.*;

/**
 * class for BFS
 * @author Yingqin Fu
 *
 */
public class FS {
    public static void main(String[] args) {
    	int[][] G1= {
    			{0,1,0,1,0,0},
				{0,0,1,0,0,0},
				{0,0,0,0,0,1},
				{0,0,0,0,1,0},
				{0,0,0,0,0,1},
				{0,0,0,0,0,0}
		 };
    	
    	int[][] G2= {
    			{0,1,1,0,0},
    			{0,0,0,1,1},
    			{0,0,0,0,0},
    			{0,0,0,0,0},
    			{0,0,0,0,0}
    	};
    	int[][] G3= {
    			{0,1,0},
    			{0,0,0},
    			{0,0,0}
    	};
    	Node[] node = new Node[G2.length];
		
		//init graph
		Function.init(node,G2);
		
        ArrayList<Node> nodes = new ArrayList<>();
      
        ArrayList<Node> z2= BFS(node[0],nodes);
        Function.print(z2);
        System.out.println("-----------");
        ArrayList<Node> z=BFS(node[4],z2);
        Function.print(z);
       
      
    }
    /**
     * BFS, and get the marked nodes of a node menge
     * @param v 
     * @param nodes
     */
    public static ArrayList<Node> BFS(Node v, ArrayList<Node> nodes) {
        Set<Node> visited = new HashSet<>();
        ArrayList<Node> marked = new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();
        
        // offer to queue
        queue.offer(v);    
        while (!queue.isEmpty()) {
        	// poll from queue
            Node node = queue.poll();   
            if (!visited.contains(node)) {
                //System.out.println("This node is visited:" + node.getName());       
                visited.add(node);
              
               if(!nodes.contains(node)||node==v) {
            	   for (Node next : node.getRelationNodes()) {
                       queue.offer(next);
                   }
               }
               //
               else if(nodes.contains(node)) {
            	   marked.add(node);
               }
                
                
            }
        }
        
        return marked;
    }
    
    /**
     * DFS
     *
     * @param v       
     * @param visited all visited nodes
     */
    public static void DFS(Node v, ArrayList<Node> visited) {
        if (visited.contains(v)) 
            return;
        
        else {
      
           
            visited.add(v);
            
            for (Node next : v.getRelationNodes()) {
                DFS(next, visited);
            }
        }
    }
}
