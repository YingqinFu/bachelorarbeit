package test;

import java.util.ArrayList;

public class FindMinCost {
	public static void main(String[] args) {
		
		int[][] G=DAGGenerator.DagGenerator(1500,300);
		Node[] node = new Node[G.length];
		
		Function.init(node, G);
	
		long start=System.currentTimeMillis();
		
     	Function.print(FindMinCostSep(G,0,400,node));
	      
		 long end=System.currentTimeMillis(); 
		System.out.println("Runtime is�� "+(end-start)+"ms");   
		
			  
		
	}
	public static ArrayList<Node> FindMinCostSep(int[][] G,int X,int Y,Node[] node){
		
			Node[] z= {node[X],node[Y]};
		
	       Node[] Z1=Ancestor.findAn(node,G,z);
	       System.out.println("The number of ancestors of x and y is " +Z1.length);
	       
	       //get the induce graph of G
			int[][] AntGraph=Ancestor.PaGraph(G,Z1);
			
			
			  int[][] augmentedGraph=Function.moralGraph(AntGraph);
			
			  Node[] nodes=new Node[2*G.length];
			  
			  Function.init(nodes, augmentedGraph);
			  
			  System.out.println("The minimum cost separator menge between "+X+ " and "+Y+" is: ");
			  
			  return MinCutSet(augmentedGraph,X,Y,nodes);
		
	}
	

	
	public static  ArrayList<Node> MinCutSet(int[][] G,int X,int Y,Node[] nodes){
		 
		int[][] t=MaxFlowFordFulkerson.transformation(G,X,Y,nodes);
			
		int x= MaxFlowFordFulkerson.maxFlow(t,X,2*Y+1 ,nodes, X,2*Y+1);
		
		
		
		if(x==Integer.MAX_VALUE) {
			MaxFlowFordFulkerson.knote.clear();
			
			return null;
		}
		 
		else {
		
			return MaxFlowFordFulkerson.knote;
		}
		
	}
	
	
	
}
