package test;
import java.util.ArrayList;



public class MaxFlowFordFulkerson {

	public static ArrayList<Node> knote = new ArrayList<>();
	
	
	/**
	 * get the maxFlow 
	 * @param cap
	 * @param s
	 * @param t
	 * @param node
	 * @return
	 */
	public static int maxFlow(int[][] capacity, int s, int t,Node[] node,int start,int end) {
	    for (int flow = 0;;) {
	      int minCap = findPath(capacity, new boolean[capacity.length], s, t, Integer.MAX_VALUE,node,start,end);
	      
	      if (minCap == 0)
	        return flow;
	     
	      flow += minCap;
	      
	    }
	  }
	/**
	 * find the cost of every kante
	 * @param capacity
	 * @param vis
	 * @param u
	 * @param t
	 * @param f
	 * @param node
	 * @return
	 */
	public  static int findPath(int[][] capacity, boolean[] visit, int s, int t, int f,Node[] node,int start,int end) {
		 
	    if (s == t) {
	      return f;
	    }
	    visit[s] = true;
	    
	    for (int v = 0; v < visit.length; v++)
	      if (!visit[v] && capacity[s][v] > 0) {
	    	  
	    	  //always get the minimal capacity
	    	  
	        int minCap = findPath(capacity, visit, v, t, Math.min(f, capacity[s][v]),node,start,end);
	        
	        if(s!=start&&s!=end&&s!=(end-1)/2&&(capacity[s][v]<f)) {
	        	
	    		 knote.add(node[s]);
	    	  }
	         
	      
	        if (minCap > 0) {
	        	capacity[s][v] -= minCap;
	        	capacity[v][s] += minCap;
	        	 
	          
	          return minCap;
	        }
	      }
	    return 0;
	  }
	/**
	 * Transform to nodes with capacity
	 * @param G
	 * @param X
	 * @param Y
	 * @return
	 */
	public static int[][] transformation(int[][] G,int X,int Y,Node[] node){
		
		int[][] capacity=new int[G.length][G.length];
		
		for(int i=0;i<capacity.length;i++) {
			for(int j=0;j<capacity.length;j++) {
			if(G[i][j]==1) {
				  capacity[i][j]=Integer.MAX_VALUE;
			   }
			}
		}
		
		int[][] Trans =new int[2*capacity.length][2*capacity.length];
		
		Function.init(node, Trans);
		
		Function.AssignCost(node, X, Y);
		
		for(int i=0;i<Trans.length;i++) {
			for(int j=0;j<Trans.length;j++) {
				
				if(i<capacity.length) {
					Trans[i][i+capacity.length]=node[i].getCost();
				}
				
				if(i<capacity.length&&j<capacity.length&&capacity[i][j]>0) {
					Trans[i+capacity.length][j]=capacity[i][j];
				}
				
			}
		}
		return Trans;
		
	}

	 
	  public static void main(String[] args) {
	  
		  int[][] G= DAGGenerator.DagGenerator(50,20);
		
		
		
		  
		Node[] node=new Node[2*G.length];
		 
		int[][] augmentedGraph=Function.moralGraph(G);
		 
		int[][] t=transformation(augmentedGraph,0,5,node);
		
		int x=maxFlow(t, 0, 11,node,0,11);
		
		System.out.println("The max flow is "+x);
	  
	  	System.out.println("-------------");
	  if(x==Integer.MAX_VALUE) {
		  knote.clear();
	  }
	  	Function.print(knote);
	
	  }
}
