package test;

import java.util.ArrayList;

import test.Node;

/**
 *  class for Node  
 */
public class Node{
	/**
	 * the name of a node
	 */
	public String name = null;
	/**
	 * the neighborhood of a node
	 */
	public ArrayList<Node> relationNodes = new ArrayList<Node>();
	
	public ArrayList<Node> father = new ArrayList<Node>();
	
	public ArrayList<Node> son = new ArrayList<Node>();
	/**
	 * getter of name
	 * @return
	 */
	public String getName() {
		return name;
	}
	/**
	 * the cost of a node
	 */
	public int cost;
	/**
	 * setter of name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * getter of relation nodes
	 * @return
	 */
	public ArrayList<Node> getRelationNodes() {
		return relationNodes;
	}
	/**
	 * setter of relation nodes
	 * @param relationNodes
	 */
	public void setRelationNodes(ArrayList<Node> relationNodes) {
		this.relationNodes = relationNodes;
	}
	/**
	 * getter of cost
	 * @return
	 */
	public int getCost() {
		return cost;
		
	}
	
	/**
	 * setter of cost
	 */
	public void setCost(int cost) {
		this.cost=cost;
	}
	
	public ArrayList<Node> getFather(){
		return father;
	}
	
	public void setFather(ArrayList<Node> Father) {
		this.father = Father;
	}
	
	public ArrayList<Node> getSon(){
		return son;
	}
	
	public void setSon(ArrayList<Node> son) {
		this.father = son;
	}
	
	
}



