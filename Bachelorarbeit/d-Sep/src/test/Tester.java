package test;

public class Tester {
	 public static void main(String[] args) {
		 
			int[][] dag=dag(1000,7);
			
			Function.print(dag);
			
			
			 
		}
		 
		 public static int[][] dag(int n, int m){
			 
			 	int[][] graph = new int[n][n];
				
				int edgs=0;
				
				while(edgs<m) {
					
					int index1=(int)(Math.random()*graph.length);
					
					int index2=(int)(Math.random()*graph.length);
					
					if(index1<index2&&graph[index1][index2]==0) {
						
						graph[index1][index2]=1;
						edgs++;
					}
				}
			 
			return graph;
			 
		 }
		 
}
